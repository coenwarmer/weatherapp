# WeatherApp
By Coen Warmer (@coenwarmer)


## Technologies used

* Webpack 2
* Babel
* Angular 1.5.11
* Angular UI Router
* Angular Animate
* Sass


## How to run

First check out the repo, then:

`npm install` or `yarn` (you need Node >5.10 if you want to use Yarn)

After completion, run:

`npm run dev`

This will start a local server, open a browser and immediately open the app.


## Choices made:

All app components are ES6 modules that are bundled by Webpack. Modules can be exported and imported, allowing easy code reuse between modules.

Any API endpoint resources that are needed to display a certain route are resolved before navigating to route. In this use case, this ensures that the user can immediately interact with the view once it is transitioned. In other use cases where more API interactions are needed, only the required resources could be resolved on enter, and the rest could be loaded in lazily.

All generic things such as reset stylesheet, button styles, grid styles are in their own scss files to ensure easy extendability.
