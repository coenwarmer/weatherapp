export default {
    Amsterdam: {
        id: 2759794,
        img: 'https://a6d083dea07dca1c73fc-cb58174e03923271eac4a1b3e4a70779.ssl.cf2.rackcdn.com/uploads/city_info/file/0004/amsterdam-02.jpg'
    },
    Cardiff: {
        id: 2172349,
        img: 'http://webmedia.jcu.edu/wordpress101/files/2015/07/Cardiff-Co-033.jpg'
    },
    Paris: {
        id: 6618607,
        img:'https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Louvre_Museum_Wikimedia_Commons.jpg/800px-Louvre_Museum_Wikimedia_Commons.jpg'
    },
    Lisbon: {
        id: 4516749,
        img: 'https://image.jimcdn.com/app/cms/image/transf/dimension=1920x400:format=jpg/path/sa6549607c78f5c11/image/i5cc400aaff0b5f21/version/1471521184/image.jpg'
    },
    Rome: {
        id: 4908066,
        img: 'http://www.telegraph.co.uk/content/dam/Travel/Tours/rome-large.jpg'
    }
};
