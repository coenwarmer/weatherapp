export default ( ) => {

    return {
    'cod': '200',
    'message': 0.0031,
    'cnt': 35,
    'list': [
        {
            'dt': 1488466800,
            'main': {
                'temp': 280.67,
                'temp_min': 279.932,
                'temp_max': 280.67,
                'pressure': 1021.9,
                'sea_level': 1021.8,
                'grnd_level': 1021.9,
                'humidity': 91,
                'temp_kf': 0.74
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10d'
                }
            ],
            'clouds': {
                'all': 80
            },
            'wind': {
                'speed': 10.38,
                'deg': 264.002
            },
            'rain': {
                '3h': 0.42
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-02 15:00:00'
        },
        {
            'dt': 1488477600,
            'main': {
                'temp': 279.72,
                'temp_min': 279.162,
                'temp_max': 279.72,
                'pressure': 1023.11,
                'sea_level': 1023.11,
                'grnd_level': 1023.11,
                'humidity': 87,
                'temp_kf': 0.55
            },
            'weather': [
                {
                    'id': 800,
                    'main': 'Clear',
                    'description': 'clear sky',
                    'icon': '01n'
                }
            ],
            'clouds': {
                'all': 0
            },
            'wind': {
                'speed': 8.28,
                'deg': 246.501
            },
            'rain': {},
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-02 18:00:00'
        },
        {
            'dt': 1488488400,
            'main': {
                'temp': 279.81,
                'temp_min': 279.439,
                'temp_max': 279.81,
                'pressure': 1023.47,
                'sea_level': 1023.35,
                'grnd_level': 1023.47,
                'humidity': 91,
                'temp_kf': 0.37
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 48
            },
            'wind': {
                'speed': 8.05,
                'deg': 234.001
            },
            'rain': {
                '3h': 0.030000000000001
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-02 21:00:00'
        },
        {
            'dt': 1488499200,
            'main': {
                'temp': 279.68,
                'temp_min': 279.499,
                'temp_max': 279.68,
                'pressure': 1022.67,
                'sea_level': 1022.55,
                'grnd_level': 1022.67,
                'humidity': 90,
                'temp_kf': 0.18
            },
            'weather': [
                {
                    'id': 802,
                    'main': 'Clouds',
                    'description': 'scattered clouds',
                    'icon': '03n'
                }
            ],
            'clouds': {
                'all': 36
            },
            'wind': {
                'speed': 8.22,
                'deg': 222.5
            },
            'rain': {},
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-03 00:00:00'
        },
        {
            'dt': 1488510000,
            'main': {
                'temp': 278.84,
                'temp_min': 278.84,
                'temp_max': 278.84,
                'pressure': 1020.49,
                'sea_level': 1020.35,
                'grnd_level': 1020.49,
                'humidity': 91,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 64
            },
            'wind': {
                'speed': 7.36,
                'deg': 217
            },
            'rain': {},
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-03 03:00:00'
        },
        {
            'dt': 1488520800,
            'main': {
                'temp': 277.917,
                'temp_min': 277.917,
                'temp_max': 277.917,
                'pressure': 1018.3,
                'sea_level': 1018.21,
                'grnd_level': 1018.3,
                'humidity': 93,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 64
            },
            'wind': {
                'speed': 6.41,
                'deg': 205.001
            },
            'rain': {},
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-03 06:00:00'
        },
        {
            'dt': 1488531600,
            'main': {
                'temp': 278.768,
                'temp_min': 278.768,
                'temp_max': 278.768,
                'pressure': 1016.27,
                'sea_level': 1016.23,
                'grnd_level': 1016.27,
                'humidity': 96,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 6.01,
                'deg': 195.505
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-03 09:00:00'
        },
        {
            'dt': 1488542400,
            'main': {
                'temp': 279.638,
                'temp_min': 279.638,
                'temp_max': 279.638,
                'pressure': 1013.53,
                'sea_level': 1013.5,
                'grnd_level': 1013.53,
                'humidity': 100,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10d'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 4.48,
                'deg': 182.001
            },
            'rain': {
                '3h': 1.21
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-03 12:00:00'
        },
        {
            'dt': 1488553200,
            'main': {
                'temp': 280.077,
                'temp_min': 280.077,
                'temp_max': 280.077,
                'pressure': 1010.47,
                'sea_level': 1010.39,
                'grnd_level': 1010.47,
                'humidity': 98,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 3.16,
                'deg': 176.509
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-03 15:00:00'
        },
        {
            'dt': 1488564000,
            'main': {
                'temp': 281.106,
                'temp_min': 281.106,
                'temp_max': 281.106,
                'pressure': 1008.56,
                'sea_level': 1008.45,
                'grnd_level': 1008.56,
                'humidity': 93,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 501,
                    'main': 'Rain',
                    'description': 'moderate rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 4.31,
                'deg': 190.502
            },
            'rain': {
                '3h': 7.32
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-03 18:00:00'
        },
        {
            'dt': 1488574800,
            'main': {
                'temp': 282.329,
                'temp_min': 282.329,
                'temp_max': 282.329,
                'pressure': 1007.29,
                'sea_level': 1007.28,
                'grnd_level': 1007.29,
                'humidity': 91,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 802,
                    'main': 'Clouds',
                    'description': 'scattered clouds',
                    'icon': '03n'
                }
            ],
            'clouds': {
                'all': 48
            },
            'wind': {
                'speed': 6.11,
                'deg': 202
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-03 21:00:00'
        },
        {
            'dt': 1488585600,
            'main': {
                'temp': 282.064,
                'temp_min': 282.064,
                'temp_max': 282.064,
                'pressure': 1006.67,
                'sea_level': 1006.64,
                'grnd_level': 1006.67,
                'humidity': 91,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 80
            },
            'wind': {
                'speed': 4.97,
                'deg': 194.004
            },
            'rain': {
                '3h': 0.07
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-04 00:00:00'
        },
        {
            'dt': 1488596400,
            'main': {
                'temp': 282.265,
                'temp_min': 282.265,
                'temp_max': 282.265,
                'pressure': 1005.44,
                'sea_level': 1005.35,
                'grnd_level': 1005.44,
                'humidity': 93,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 88
            },
            'wind': {
                'speed': 5.01,
                'deg': 195.509
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-04 03:00:00'
        },
        {
            'dt': 1488607200,
            'main': {
                'temp': 282.272,
                'temp_min': 282.272,
                'temp_max': 282.272,
                'pressure': 1005.19,
                'sea_level': 1005.13,
                'grnd_level': 1005.19,
                'humidity': 94,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 6.32,
                'deg': 196.001
            },
            'rain': {
                '3h': 1.9
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-04 06:00:00'
        },
        {
            'dt': 1488618000,
            'main': {
                'temp': 282.735,
                'temp_min': 282.735,
                'temp_max': 282.735,
                'pressure': 1004.7,
                'sea_level': 1004.65,
                'grnd_level': 1004.7,
                'humidity': 95,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 76
            },
            'wind': {
                'speed': 6.16,
                'deg': 176.002
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-04 09:00:00'
        },
        {
            'dt': 1488628800,
            'main': {
                'temp': 283.852,
                'temp_min': 283.852,
                'temp_max': 283.852,
                'pressure': 1004.24,
                'sea_level': 1004.24,
                'grnd_level': 1004.24,
                'humidity': 97,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10d'
                }
            ],
            'clouds': {
                'all': 36
            },
            'wind': {
                'speed': 6.07,
                'deg': 187.502
            },
            'rain': {
                '3h': 0.19
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-04 12:00:00'
        },
        {
            'dt': 1488639600,
            'main': {
                'temp': 283.801,
                'temp_min': 283.801,
                'temp_max': 283.801,
                'pressure': 1003.39,
                'sea_level': 1003.37,
                'grnd_level': 1003.39,
                'humidity': 97,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 76
            },
            'wind': {
                'speed': 3.22,
                'deg': 189.503
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-04 15:00:00'
        },
        {
            'dt': 1488650400,
            'main': {
                'temp': 282.633,
                'temp_min': 282.633,
                'temp_max': 282.633,
                'pressure': 1002.83,
                'sea_level': 1002.82,
                'grnd_level': 1002.83,
                'humidity': 97,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 1.82,
                'deg': 105.503
            },
            'rain': {
                '3h': 1.7225
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-04 18:00:00'
        },
        {
            'dt': 1488661200,
            'main': {
                'temp': 281.973,
                'temp_min': 281.973,
                'temp_max': 281.973,
                'pressure': 1002.25,
                'sea_level': 1002.18,
                'grnd_level': 1002.25,
                'humidity': 98,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 2.66,
                'deg': 56.5009
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-04 21:00:00'
        },
        {
            'dt': 1488672000,
            'main': {
                'temp': 281.66,
                'temp_min': 281.66,
                'temp_max': 281.66,
                'pressure': 1002.07,
                'sea_level': 1002.16,
                'grnd_level': 1002.07,
                'humidity': 98,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 501,
                    'main': 'Rain',
                    'description': 'moderate rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 2.41,
                'deg': 6.00836
            },
            'rain': {
                '3h': 7.1875
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-05 00:00:00'
        },
        {
            'dt': 1488682800,
            'main': {
                'temp': 280.782,
                'temp_min': 280.782,
                'temp_max': 280.782,
                'pressure': 1003.92,
                'sea_level': 1003.9,
                'grnd_level': 1003.92,
                'humidity': 99,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 5.36,
                'deg': 287.504
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-05 03:00:00'
        },
        {
            'dt': 1488693600,
            'main': {
                'temp': 279.313,
                'temp_min': 279.313,
                'temp_max': 279.313,
                'pressure': 1006.94,
                'sea_level': 1006.96,
                'grnd_level': 1006.94,
                'humidity': 100,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 501,
                    'main': 'Rain',
                    'description': 'moderate rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 88
            },
            'wind': {
                'speed': 5.86,
                'deg': 249.5
            },
            'rain': {
                '3h': 5.6875
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-05 06:00:00'
        },
        {
            'dt': 1488704400,
            'main': {
                'temp': 279.564,
                'temp_min': 279.564,
                'temp_max': 279.564,
                'pressure': 1011.36,
                'sea_level': 1011.28,
                'grnd_level': 1011.36,
                'humidity': 100,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 88
            },
            'wind': {
                'speed': 7.37,
                'deg': 309
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-05 09:00:00'
        },
        {
            'dt': 1488715200,
            'main': {
                'temp': 279.448,
                'temp_min': 279.448,
                'temp_max': 279.448,
                'pressure': 1016.02,
                'sea_level': 1015.99,
                'grnd_level': 1016.02,
                'humidity': 100,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10d'
                }
            ],
            'clouds': {
                'all': 76
            },
            'wind': {
                'speed': 7.31,
                'deg': 291.503
            },
            'rain': {
                '3h': 0.45
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-05 12:00:00'
        },
        {
            'dt': 1488726000,
            'main': {
                'temp': 279.317,
                'temp_min': 279.317,
                'temp_max': 279.317,
                'pressure': 1018.57,
                'sea_level': 1018.46,
                'grnd_level': 1018.57,
                'humidity': 96,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 88
            },
            'wind': {
                'speed': 6.57,
                'deg': 273.002
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-05 15:00:00'
        },
        {
            'dt': 1488736800,
            'main': {
                'temp': 278.276,
                'temp_min': 278.276,
                'temp_max': 278.276,
                'pressure': 1019.87,
                'sea_level': 1019.77,
                'grnd_level': 1019.87,
                'humidity': 94,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 80
            },
            'wind': {
                'speed': 4.83,
                'deg': 244.5
            },
            'rain': {},
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-05 18:00:00'
        },
        {
            'dt': 1488747600,
            'main': {
                'temp': 277.852,
                'temp_min': 277.852,
                'temp_max': 277.852,
                'pressure': 1020.25,
                'sea_level': 1020.21,
                'grnd_level': 1020.25,
                'humidity': 95,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 804,
                    'main': 'Clouds',
                    'description': 'overcast clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 92
            },
            'wind': {
                'speed': 3.26,
                'deg': 207.012
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-05 21:00:00'
        },
        {
            'dt': 1488758400,
            'main': {
                'temp': 277.75,
                'temp_min': 277.75,
                'temp_max': 277.75,
                'pressure': 1021,
                'sea_level': 1020.86,
                'grnd_level': 1021,
                'humidity': 98,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10n'
                }
            ],
            'clouds': {
                'all': 80
            },
            'wind': {
                'speed': 1.5,
                'deg': 150.502
            },
            'rain': {
                '3h': 0.024999999999999
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-06 00:00:00'
        },
        {
            'dt': 1488769200,
            'main': {
                'temp': 276.497,
                'temp_min': 276.497,
                'temp_max': 276.497,
                'pressure': 1022.18,
                'sea_level': 1022.19,
                'grnd_level': 1022.18,
                'humidity': 95,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 802,
                    'main': 'Clouds',
                    'description': 'scattered clouds',
                    'icon': '03n'
                }
            ],
            'clouds': {
                'all': 32
            },
            'wind': {
                'speed': 1.31,
                'deg': 5.50211
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-06 03:00:00'
        },
        {
            'dt': 1488780000,
            'main': {
                'temp': 275.954,
                'temp_min': 275.954,
                'temp_max': 275.954,
                'pressure': 1025.3,
                'sea_level': 1025.23,
                'grnd_level': 1025.3,
                'humidity': 92,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 64
            },
            'wind': {
                'speed': 2.1,
                'deg': 301.5
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-06 06:00:00'
        },
        {
            'dt': 1488790800,
            'main': {
                'temp': 278.978,
                'temp_min': 278.978,
                'temp_max': 278.978,
                'pressure': 1028.24,
                'sea_level': 1028.2,
                'grnd_level': 1028.24,
                'humidity': 98,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 801,
                    'main': 'Clouds',
                    'description': 'few clouds',
                    'icon': '02d'
                }
            ],
            'clouds': {
                'all': 20
            },
            'wind': {
                'speed': 2.87,
                'deg': 292.503
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-06 09:00:00'
        },
        {
            'dt': 1488801600,
            'main': {
                'temp': 280.397,
                'temp_min': 280.397,
                'temp_max': 280.397,
                'pressure': 1029.28,
                'sea_level': 1029.18,
                'grnd_level': 1029.28,
                'humidity': 100,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 500,
                    'main': 'Rain',
                    'description': 'light rain',
                    'icon': '10d'
                }
            ],
            'clouds': {
                'all': 32
            },
            'wind': {
                'speed': 3.42,
                'deg': 287.003
            },
            'rain': {
                '3h': 0.024999999999999
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-06 12:00:00'
        },
        {
            'dt': 1488812400,
            'main': {
                'temp': 279.917,
                'temp_min': 279.917,
                'temp_max': 279.917,
                'pressure': 1029.52,
                'sea_level': 1029.43,
                'grnd_level': 1029.52,
                'humidity': 95,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04d'
                }
            ],
            'clouds': {
                'all': 64
            },
            'wind': {
                'speed': 3.22,
                'deg': 270.5
            },
            'sys': {
                'pod': 'd'
            },
            'dt_txt': '2017-03-06 15:00:00'
        },
        {
            'dt': 1488823200,
            'main': {
                'temp': 277.744,
                'temp_min': 277.744,
                'temp_max': 277.744,
                'pressure': 1029.79,
                'sea_level': 1029.76,
                'grnd_level': 1029.79,
                'humidity': 93,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 56
            },
            'wind': {
                'speed': 1.31,
                'deg': 246.501
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-06 18:00:00'
        },
        {
            'dt': 1488834000,
            'main': {
                'temp': 276.156,
                'temp_min': 276.156,
                'temp_max': 276.156,
                'pressure': 1029.95,
                'sea_level': 1029.85,
                'grnd_level': 1029.95,
                'humidity': 91,
                'temp_kf': 0
            },
            'weather': [
                {
                    'id': 803,
                    'main': 'Clouds',
                    'description': 'broken clouds',
                    'icon': '04n'
                }
            ],
            'clouds': {
                'all': 68
            },
            'wind': {
                'speed': 1.31,
                'deg': 62.0014
            },
            'sys': {
                'pod': 'n'
            },
            'dt_txt': '2017-03-06 21:00:00'
        }
    ],
    'city': {
        'id': 2759794,
        'name': 'Amsterdam',
        'coord': {
            'lat': 52.374,
            'lon': 4.8897
        },
        'country': 'NL'
    }
}
}