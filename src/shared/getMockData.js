export default ( ) => {

    return {
        'cnt': 5,
        'list': [
            {
                'coord': {
                    'lon': 4.89,
                    'lat': 52.37
                },
                'sys': {
                    'type': 1,
                    'id': 5204,
                    'message': 0.1636,
                    'country': 'NL',
                    'sunrise': 1488435751,
                    'sunset': 1488475339
                },
                'weather': [
                    {
                        'id': 302,
                        'main': 'Drizzle',
                        'description': 'heavy intensity drizzle',
                        'icon': '09d'
                    }
                ],
                'main': {
                    'temp': 7.24,
                    'pressure': 1006,
                    'humidity': 70,
                    'temp_min': 6,
                    'temp_max': 8
                },
                'visibility': 10000,
                'wind': {
                    'speed': 10.8,
                    'deg': 250
                },
                'clouds': {
                    'all': 75
                },
                'dt': 1488456240,
                'id': 2759794,
                'name': 'Amsterdam'
            },
            {
                'coord': {
                    'lon': 151.67,
                    'lat': -32.95
                },
                'sys': {
                    'type': 1,
                    'id': 8242,
                    'message': 0.1879,
                    'country': 'AU',
                    'sunrise': 1488397383,
                    'sunset': 1488443264
                },
                'weather': [
                    {
                        'id': 803,
                        'main': 'Clouds',
                        'description': 'broken clouds',
                        'icon': '04n'
                    }
                ],
                'main': {
                    'temp': 21,
                    'pressure': 1015,
                    'humidity': 94,
                    'temp_min': 21,
                    'temp_max': 21
                },
                'visibility': 10000,
                'wind': {
                    'speed': 3.05,
                    'deg': 207.002
                },
                'clouds': {
                    'all': 68
                },
                'dt': 1488454200,
                'id': 2172349,
                'name': 'Cardiff'
            },
            {
                'coord': {
                    'lon': 2.34,
                    'lat': 48.86
                },
                'sys': {
                    'type': 1,
                    'id': 5610,
                    'message': 0.1858,
                    'country': 'FR',
                    'sunrise': 1488436132,
                    'sunset': 1488476180
                },
                'weather': [
                    {
                        'id': 803,
                        'main': 'Clouds',
                        'description': 'broken clouds',
                        'icon': '04d'
                    }
                ],
                'main': {
                    'temp': 10.83,
                    'pressure': 1017,
                    'humidity': 50,
                    'temp_min': 10,
                    'temp_max': 12
                },
                'visibility': 10000,
                'wind': {
                    'speed': 6.2,
                    'deg': 260,
                    'var_beg': 230,
                    'var_end': 290
                },
                'clouds': {
                    'all': 75
                },
                'dt': 1488456000,
                'id': 6618607,
                'name': 'Paris 01'
            },
            {
                'coord': {
                    'lon': -83.64,
                    'lat': 39.86
                },
                'sys': {
                    'type': 1,
                    'id': 2193,
                    'message': 0.1844,
                    'country': 'US',
                    'sunrise': 1488456278,
                    'sunset': 1488497298
                },
                'weather': [
                    {
                        'id': 804,
                        'main': 'Clouds',
                        'description': 'overcast clouds',
                        'icon': '04n'
                    }
                ],
                'main': {
                    'temp': 1.01,
                    'pressure': 1019,
                    'humidity': 69,
                    'temp_min': 0,
                    'temp_max': 2
                },
                'visibility': 16093,
                'wind': {
                    'speed': 5.7,
                    'deg': 290
                },
                'clouds': {
                    'all': 90
                },
                'dt': 1488455100,
                'id': 4516749,
                'name': 'Lisbon'
            },
            {
                'coord': {
                    'lon': -89.5,
                    'lat': 40.88
                },
                'sys': {
                    'type': 1,
                    'id': 981,
                    'message': 0.1842,
                    'country': 'US',
                    'sunrise': 1488457730,
                    'sunset': 1488498661
                },
                'weather': [
                    {
                        'id': 800,
                        'main': 'Clear',
                        'description': 'sky is clear',
                        'icon': '01n'
                    }
                ],
                'main': {
                    'temp': -2.26,
                    'pressure': 1023,
                    'humidity': 79,
                    'temp_min': -3,
                    'temp_max': -2
                },
                'visibility': 16093,
                'wind': {
                    'speed': 3.1,
                    'deg': 280
                },
                'clouds': {
                    'all': 1
                },
                'dt': 1488455760,
                'id': 4908066,
                'name': 'Rome'
            }
        ]
    }
}