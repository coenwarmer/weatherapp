import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import weatherNavModule from './../weatherNav';
import './reset.scss';
import './styles.scss';
import './transitions.scss';
import './buttons.scss';
import './grid.scss';

export default
	angular.module('cwb.weather.weatherApp', [
		angularUiRouterModule,
		weatherNavModule,
	])
		.directive('weatherApp', [ ( ) => {

			return {
				restrict: 'E',
				template: '<weather-nav></weather-nav><div class="ui-view-container"><ui-view></ui-view></div>'
			};

		}])
		.name;
