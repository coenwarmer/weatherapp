import angular from 'angular';
import template from './template.html';
import './styles.scss';

export default
	angular.module('cwb.weather.weatherNav', [
	])
		.directive('weatherNav', [ '$window', '$state', '$rootScope', ( $window, $state, $rootScope ) => {

			return {
				restrict: 'E',
				template,
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.goBack = ( ) => $window.history.back();

					$rootScope.$on('$stateChangeSuccess', ( ...rest ) => {
						ctrl.isDetailState = ( ) => $state.current.name === 'cityDetail';
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
