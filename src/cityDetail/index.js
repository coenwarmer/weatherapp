import angular from 'angular';
import cityDetailViewModule from './cityDetailView';
import getWeatherAPIKey from './../shared/getWeatherAPIKey';

export default {
	moduleName:
        angular.module('cwb.weather.cityDetail', [
            cityDetailViewModule
        ])
		.controller('cityController', [ '$stateParams', '$scope', 'weatherData', ( $stateParams, $scope, weatherData ) => {

			$scope.weatherData = weatherData;

		}])
        .name,
	config: [
		{
			route: {
				url: '/city/:cityID',
				template: '<city-detail-view weather-data="weatherData"></city-detail-view>',
				resolve: {
					weatherData: [ '$http', '$stateParams', ( $http, $stateParams ) => {

                        return $http.get(`http://api.openweathermap.org/data/2.5/forecast?id=${$stateParams.cityID}&APPID=${getWeatherAPIKey()}`)
                            .then( result => {
                                return result.data.list;
                            })
                            .catch( err => {
                                console.log('error', err);
                            });

					}]
				},
				controller: 'cityController'
			},
			state: 'cityDetail'
		}
	]
};

