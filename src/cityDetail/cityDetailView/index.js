import angular from 'angular';
import template from './template.html';
import cities from '../../shared/cities';
import mapValues from 'lodash/mapValues';
import invert from 'lodash/invert';
import './styles.scss';

export default
	angular.module('cwb.weather.cityDetailView', [
	])
		.directive('cityDetailView', [ '$stateParams', ( $stateParams ) => {

			return {
				restrict: 'E',
				template,
                scope: {
                    weatherData: '&'
                },
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

                    ctrl.getForecast = ( ) => {
						return ctrl.weatherData().filter( item => {
							return item.dt_txt.substr(item.dt_txt.length - 8) === '09:00:00';
						});
					};

					ctrl.getCity = ( ) => invert(mapValues(cities, ( item ) => item.id ))[$stateParams.cityID];

					ctrl.getCityImage = ( ) => mapValues(cities, ( item ) => item.img )[ctrl.getCity()];

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
