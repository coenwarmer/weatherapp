import angular from 'angular';
import uiRouter from 'angular-ui-router';
import weatherAppModule from './shared/weatherApp';
import citiesList from './citiesList';
import cityDetail from './cityDetail';
import flatten from 'lodash/flatten';
import merge from 'lodash/merge';

export default angular.module('cwb.weather.routing', [
	uiRouter,
	weatherAppModule,
	citiesList.moduleName,
	cityDetail.moduleName
])
	.config([ '$stateProvider', '$urlRouterProvider', '$locationProvider', '$urlMatcherFactoryProvider', ( $stateProvider, $urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider ) => {

		$locationProvider.html5Mode(true);

		$urlMatcherFactoryProvider.strictMode(false);

		$urlRouterProvider.otherwise(( $injector ) => {

			let $state = $injector.get('$state');

			$state.go('citiesList');

		});

		$stateProvider.state({
			name: 'root',
			template: '<weather-app></weather-app>'
		});

		flatten(
			[ citiesList, cityDetail ]
				.map(routeConfig => routeConfig.config)
			)
			.forEach(route => {

				let mergedState =
					merge(route.route, {
						parent: 'root'
					});

				$stateProvider.state(route.state, mergedState);

			});

	}])
	.run([ '$rootScope', ( $rootScope ) => {

		$rootScope.$on('$stateChangeError', ( ...rest ) => {
			console.log(...rest);
		});

	}])
	.name;
