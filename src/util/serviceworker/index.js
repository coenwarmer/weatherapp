import angular from 'angular';
import { Promise } from 'es6-promise';
import once from 'lodash/once';

const FILENAME = 'service-worker.js';

export default
	angular.module('cwb.weather.util.serviceworker', [
		])
		.factory('serviceWorker', [ '$window', '$rootScope', ( $window, $rootScope ) => {

			return ( scope = './' ) => {

				let serviceWorker = {},
					supported =
					('serviceWorker' in navigator
						&& (window.location.protocol === 'https:'
						|| window.location.hostname === 'localhost'
						|| window.location.hostname.indexOf('127.') === 0)
					),
					registrationPromise,
					enabled;

				let register = ( ) => {
					registrationPromise =
						navigator.serviceWorker.register(scope + FILENAME, {
							scope
						});

					return registrationPromise;
				};

				let emitNewVersion = ( ) => {

					console.log('a new version of the app is available.');

				};

				let getRegistration = ( ) => navigator.serviceWorker.getRegistration();

				serviceWorker.isEnabled = ( ) => {
					return getRegistration(scope)
						.then( ( registration ) => {
							return !!registration;
						});
				};

				serviceWorker.enable = ( ) => {

					return register()
						.catch( err => {
							// console.error('Error installing ServiceWorker', err);
						});
				};

				serviceWorker.disable = ( ) => {

					let promises = [];

					if (enabled === false) {
						return serviceWorker.clear();
					}

					promises.push(serviceWorker.clear());

					promises.push(
						navigator.serviceWorker.getRegistration()
							.then(( registration ) => {
								if (registration) {
									return registration.unregister();
								}
							})
					);

					return Promise.all(promises);
				};

				serviceWorker.clear = ( ) => {
					return $window.caches.keys().then(( cacheNames ) => {
						return Promise.all(
							cacheNames.map(( cacheName ) => {
								return $window.caches.delete(cacheName);
							})
						);
					});
				};

				serviceWorker.isSupported = ( ) => supported;

				if (serviceWorker.isSupported()) {

					serviceWorker.isEnabled()
						.then( ( swEnabled ) => {
							if (swEnabled) {
								return getRegistration(scope);
							}

							return null;
						})
						.then( ( registration ) => {

							if (!registration) {
								return;
							}

							registration.onupdatefound = once(( ) => {
								$rootScope.$apply(( ) => {
									emitNewVersion();
								});
							});
							
							if (typeof registration.update === 'function') {
								registration.update();
							}
						});
				}

				return serviceWorker;

			};

			
		}])
			.name;
