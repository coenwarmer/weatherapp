import angular from 'angular';
import ngAnimate from 'angular-animate';
import routing from './routing';
import serviceworkerModule from './util/serviceworker';

export default
	angular.module('cwb.weather', [
		routing,
		ngAnimate,
		serviceworkerModule
	])
	.config(['$provide', ( $provide ) => {
		
		$provide.decorator('$exceptionHandler', [ '$delegate', ( $delegate ) => {

			return ( exception, cause ) => {
				$delegate(exception, cause);
				setTimeout(( ) => {
					console.error(exception.stack);
				});
			};

		}]);

	}])
	.run([ '$window', 'serviceWorker', '$animate', ( $window, serviceWorker, $animate ) => {

		// let enable = true, //eslint-disable-line
		// 	worker = serviceWorker();

		// $animate.enabled(true);

		// if (worker.isSupported()) {
		// 	worker.isEnabled()
		// 		.then( ( enabled ) => {
		// 			if (enabled !== enable) {
		// 				return (enable ?
		// 					worker.enable()
		// 					: worker.disable()
		// 						.then(( ) => {
		// 							if (enabled) {
		// 								$window.location.reload();
		// 							}
		// 						})
		// 				);
		// 			}
		// 		});
		// }

	}])
		.name;
