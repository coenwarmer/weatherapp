import angular from 'angular';
import citiesListViewModule from './citiesListView';
import getWeatherAPIKey from './../shared/getWeatherAPIKey';
import cities from './../shared/cities';
import map from 'lodash/map';

export default {
	moduleName:
        angular.module('cwb.weather.citiesList', [
            citiesListViewModule
        ])
		.controller('citiesController', [ '$state', '$stateParams', '$scope', 'weatherData', ( $state, $stateParams, $scope, weatherData ) => {

			$scope.weatherData = weatherData;

		}])
        .name,
	config: [
		{
			route: {
				url: '/',
				template: '<cities-list-view weather-data="weatherData"></cities-list-view>',
				resolve: {
					weatherData: [ '$rootScope', '$q', '$http', ( $rootScope, $q, $http ) => {

                        return $http.get(`http://api.openweathermap.org/data/2.5/group?id=${map(cities, (value) => value.id)}&units=metric&APPID=${getWeatherAPIKey()}`)
                            .then( result => {
                                return result.data.list;
                            })
                            .catch( err => {
                                console.log('error', err);
                            });

					}]
				},
				controller: 'citiesController'
			},
			state: 'citiesList'
		}
	]
};

