import angular from 'angular';
import template from './template.html';
import merge from 'lodash/merge';
import './styles.scss';

export default
	angular.module('cwb.weather.citiesListView', [
	])
		.directive('citiesListView', [ 'dateFilter', ( dateFilter) => {

			return {
				restrict: 'E',
				template,
                scope: {
                    weatherData: '&'
                },
				bindToController: true,
				controller: [ '$scope', function ( ) {

					let ctrl = this;

                    ctrl.getCities = ( ) => {
                        return ctrl.weatherData().map( item => {
                            return merge(
                                item,
                                {
                                    convertedSunriseDate: dateFilter(item.sys.sunrise * 1000, 'hh:mm')
                                },
                                {
                                    convertedSunsetDate: dateFilter(item.sys.sunset * 1000, 'hh:mm')
                                }
                            );
                        });
                    };

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
