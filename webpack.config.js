var webpack = require('webpack'),
    path = require('path'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    UglifyJsPlugin = require('uglify-js-plugin');

module.exports = {
    entry: {
        main: './src/index.js',
        vendors: 'angular'
    },
    output: {
        filename: '[hash].[name].js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendors'
        }),
        new HtmlWebpackPlugin({
            template: './src/index.template.html',
            filename: './index.html',
            inject: 'body'
        }),
        new webpack.HotModuleReplacementPlugin()
        // new SWPrecacheWebpackPlugin(
        //     {
        //         cacheId: 'backbase-weatherapp',
        //         filename: 'backbase-weather-app-service-worker.js',
        //         maximumFileSizeToCacheInBytes: 4194304,
        //         minify: true,
        //         runtimeCaching: [{
        //             handler: 'cacheFirst',
        //             urlPattern: /[.]*$/,
        //         }],
        //     }
        // ),
        // new UglifyJsPlugin({
        //     sourceMap: true,
        //     compress: {
        //         warnings: true
        //     }
        // })
    ],
	module: {
		rules: [
			{
				test: /.*\.js$/,
				loader: 'babel-loader',
				exclude: [ /node_modules/ ],
				query: {
					presets: [ 'babel-preset-es2015' ].map(require.resolve),
					plugins: [ 'babel-plugin-add-module-exports' ].map(require.resolve)
				}
			},
			{
				test: /.*\.html$/,
				loader: 'html-loader?removeOptionalTags=false&removeDefaultAttributes=false'
			},
			{
				test: /\.scss$/,
				// loader: env === 'production' ? ExtractTextPlugin.extract('style-loader', 'css-loader!autoprefixer-loader!sass-loader') : 'style-loader!css-loader!autoprefixer-loader!sass-loader',
                use: ['style-loader', 'css-loader', 'sass-loader'],
				exclude: /node_modules/
			},
			{
				test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?name=[name].[ext]&limit=10000'
			}
		],
    },
    devtool: 'source-map',
    devServer: {
        contentBase: './dist/',
        compress: true,
        port: 9000
    }
};
